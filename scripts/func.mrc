; Humphrey
; Functions
; All aliases within this file should start with func.

; Attempt to preserve my nickname
alias func.preservenick {
  if ($me != %nick) {
    nick %nick
    debug Preserved my nickname
  }
}

; Debug message handler
; Usage: /func.debug [-s] <message> 
; -s if severe error, which will also halt the script
alias func.debug {
  if (%debugmode == $true) {
    if ($1 == -s) {
      ; It's a severe error, throw exception
      aline 4 @DEBUG $timestamp <SEVERE> $2-
      func.exception
    }
    else {
      ; It's just a standard debug
      aline @DEBUG $timestamp $1-
    }
  }
}

; Throw exception (just an alias for halt at the moment)
alias func.exception {
  halt
}

; Same as /msg but splits long text into individual lines
alias func.msg {
  var %dest = $1
  var %text = $2-
  var %tokens = $numtok(%text, 32)
  var %i = 1
  
  while (%i <= %tokens) {
    var %line = %line $gettok(%quote, %i, 32)
    if ($len(%line) < 400) {
      msg %dest %line
      unset %line
    }
    inc %i
  }
}