; Humphrey
; Swedish chef translator
; All aliases within this should start with chef

; Translation Rules:
; - For first char
;   - ^e -> i
;   - ^o -> oo
; - For non-first chars
;   - ew -> oo
;   - e$ -> e-a
;   - f -> ff
;   - ir -> ur, or i -> ee if no ir has occurred
;   - ow -> oo or o -> u
;   - tion -> shun
;   - u -> oo
; - General rules
;   - An -> Un
;   - Au -> Oo
;   - A. -> E (A not at the end becomes E)
;   - en$ -> ee
;   - the -> zee
;   - th > tt
;   - v -> f
;   - w - v


; Translates English sentences into Swedish chef sentences
; Usage: $chef.parse(<text>)
alias chef.parse {
  var %text = $1-
  var %parsedtext
  var %numtok = $numtok(%text, 32)

  ; Loop through the string and swedish chef it
  var %i = 1
  while (%i <= %numtok) {
    ; If the last chr is a . or !, borkify it
    if ($right($gettok(%text, $calc(%i - 1), 32), 1) == $(.) || $right($gettok(%text, $calc(%i - 1), 32), 1) == $(!)) {
      var %parsedtext = $left(%parsedtext, $calc($len(%parsedtext) - 1)) $+ , $left($str(bork $+ $chr(32), $rand(1, 6)), -1) $+ ! $chef.applyrules($gettok(%text, %i, 32))
    }
    else {
      var %parsedtext = %parsedtext $chef.applyrules($gettok(%text, %i, 32))
    }
    inc %i
  }

  return %parsedtext $+ , $left($str(bork $+ $chr(32), $rand(1, 6)), -1) $+ !
}

; Apply all rules
; Usage: $chef.applyrules(<text>)
alias chef.applyrules {
  var %word = $1
  %word = $chef.e-i(%word)
  %word = $chef.o-oo(%word)
  %word = $chef.ew-oo(%word)
  ;%word = $chef.e-e-a(%word)
  ;%word = $chef.f-ff(%word)
  %word = $chef.ir-ur(%word)
  %word = $chef.ow-oo(%word)
  %word = $chef.tion-shun(%word)
  %word = $chef.u-oo(%word)
  %word = $chef.an-un(%word)
  %word = $chef.au-oo(%word)
  %word = $chef.a-e(%word)
  %word = $chef.en-ee(%word)
  ;%word = $chef.th-tt(%word)
  %word = $chef.the-zee(%word)
  %word = $chef.v-f(%word)
  %word = $chef.w-v(%word)
  return %word
}

; Rule aliases follow, each as an 
; individual function for clarity

; Replaces words that start with e to words that start with i
; Usage: $chef.e-i(<word>)
alias chef.e-i {
  var %word = $1 
  if ($left(%word, 1) == e) { return i $+ $right(%word, $calc($len(%word) - 1)) }
  else { return %word }
}

; Replaces words that start with o to words that start with oo
; Usage: $chef.o-oo(<word>)
alias chef.o-oo {
  var %word = $1 
  if ($left(%word, 1) == o) { return oo $+ $right(%word, $calc($len(%word) - 1)) }
  else { return %word }
}

; Replaces all instances of ew with oo
; Usage: $chef.ew-oo(<word>)
alias chef.ew-oo {
  var %word = $1 
  return $replacexcs(%word, ew, oo, EW, OO)
}

; Replaces words that end with e with words that end with e-a
; Usage: $chef.e-e-a(<word>)
alias chef.e-e-a {
  var %word = $1 
  if ($right(%word, 1) == e) { return $left(%word, $calc($len(%word) - 1)) $+ e-a }
  else { return %word }
}

; Replaces all instances of f with ff
; Usage: $chef.f-ff(<word>)
alias chef.f-ff {
  var %word = $1 
  return $replacexcs(%word, f, ff, F, FF)
}

; Replaces all instances of ir with ur, if no ir but has i, replace i with ee
; Usage: $chef.ir-ur(<word>)
alias chef.ir-ur {
  var %word = $1 
  if (*ir* iswm %word) { return $replacexcs(%word, ir, ur, IR, UR) }
  else { return $replacexcs(%word, i, ee, I, EE) }
}

; Replaces all instances of ow with oo, if no ow but has o, replace o with u
; Usage: $chef.ow-oo(<word>)
alias chef.ow-oo {
  var %word = $1 
  if (*ow* iswm %word) { return $replacexcs(%word, ow, oo, OW, OO) }
  else { return $replacexcs(%word, o, u, O, U) }
}

; Replaces all instances of tion with shun
; Usage: $chef.tion-shun(<word>)
alias chef.tion-shun {
  var %word = $1 
  return $replacexcs(%word, tion, shun, TION, SHUN)
}

; Replaces all instances of u with oo
; Usage: $chef.u-oo(<word>)
alias chef.u-oo {
  var %word = $1 
  return $replacexcs(%word, u, oo, U, OO)
}

; Replaces all instances of an with un
; Usage: $chef.an-un(<word>)
alias chef.an-un {
  var %word = $1 
  return $replacexcs(%word, an, un, AN, UN)
}

; Replaces all instances of au with oo
; Usage: $chef.au-oo(<word>)
alias chef.au-oo {
  var %word = $1 
  return $replacexcs(%word, au, oo, AU, OO)
}

; Replaces all instances of singular a with e
; Usage: $chef.a-e(<word>)
alias chef.a-e {
  var %word = $1 
  if (%word == a) { return e }
  else { return $1 }
}

; Replaces words that end with en with words that end with ee
; Usage: $chef.en-ee(<word>)
alias chef.en-ee {
  var %word = $1 
  if ($right(%word, 2) == en) { return $left(%word, $calc($len(%word) - 2)) $+ ee }
  else { return %word }
}

; Replaces all instances of the with zee
; Usage: $chef.the-zee(<word>)
alias chef.the-zee {
  var %word = $1 
  return $replacexcs(%word, the, zee, THE, ZEE)
}

; Replaces all instances of th with tt
; Usage: $chef.th-tt(<word>)
alias chef.th-tt {
  var %word = $1 
  return $replacexcs(%word, th, tt, TH, TT)
}

; Replaces all instances of v with ff
; Usage: $chef.v-f(<word>)
alias chef.v-f {
  var %word = $1 
  return $replacexcs(%word, v, f, V, F)
}

; Replaces all instances of w with v
; Usage: $chef.w-v(<word>)
alias chef.w-v {
  var %word = $1 
  return $replacexcs(%word, w, v, W, V)
}
