; Humphrey
; Pig Latin translator
; All aliases within this should start with piglatin

; Translation Rules:
; - Check the first char.
;   - If it is a vowel or a silent letter, -ay is appended to the end of the word.
;   - Not really sure how to check for silent letters.
;   - If it is a consonant, the first letter is removed, and then the consonant followed by -ay is appended to the end.
;     - e.g., "hen" becomes "enhay"
;
; Translates English sentences into Pig Latin sentences
; Usage: $piglatin.parse(<text>)
alias piglatin.parse {
  var %text = $1-
  var %parsedtext
  var %numtok = $numtok(%text, 32)

  ; Loop through the string and Pig Latinize it
  var %i = 1
  while (%i <= %numtok) {
    ; If the last chr is a . or !, oinkify it
    if ($right($gettok(%text, $calc(%i - 1), 32), 1) == $(.) || $right($gettok(%text, $calc(%i - 1), 32), 1) == $(!)) {
      var %parsedtext = $left(%parsedtext, $calc($len(%parsedtext) - 1)) $+ , $left($str(oink $+ $chr(32), $rand(1, 6)), -1) $+ ! $piglatin.applyrules($gettok(%text, %i, 32))
    }
    else {
      var %parsedtext = %parsedtext $piglatin.applyrules($gettok(%text, %i, 32))
    }
    inc %i
  }

  return %parsedtext $+ , $left($str(oink $+ $chr(32), $rand(1, 6)), -1) $+ !
}

; Apply all rules
; Usage: $piglatin.applyrules(<text>)
alias piglatin.applyrules {
  var %word = $1
  ; Check whether the first letter is a vowel, and if so, replace accordingly
  if ($left(%word,1) isletter a,e,i,o,u) { return $piglatin.vowel(%word) }
  ; Now we know it's not a vowel, so check whether it's a letter, and if it is, it's a consonant - replace accordingly
  else if ($left(%word,1) isalpha) && ($left(%word,1) !isletter a,e,i,o,u) { return $piglatin.consonant(%word) }
  ; Now we know it doesn't start with a letter, so just replace it as it is. 
  else { return %word }
}

; Rule aliases follow, each as an 
; individual function for clarity

; Replaces words that start with vowels
; Usage: $piglatin.vowel(<word>)
alias piglatin.vowel {
  var %word = $1 
  return %word $+ way
}

; Replaces words that start with consonants
; Usage: $piglatin.consonant(<word>)
alias piglatin.consonant {
  var %word = $1 
  if ($left(%word,1) isletter t,s,p,w,c) && ($left($right(%word,$calc($len(%word) - 1)), 1) isletter h) {
    return $right(%word, $calc($len(%word) - 2)) $+ $left(%word, 2) $+ ay
  }
  elseif ($left(%word,1) isletter q) && ($left($right(%word,$calc($len(%word) - 1)), 1) isletter u) {
    return $right(%word, $calc($len(%word) - 2)) $+ $left(%word ,2) $+ ay
  }
  else {
    return $right(%word, $calc($len(%word) - 1)) $+ $left(%word,1) $+ ay
  }
}