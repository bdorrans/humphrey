; Humphrey
; Database
; All aliases within this function should start with db.

; Open database connection
; Usage: var %db = $db.open(<dbname>)
alias db.open {
  var %db = db/ $+ $1 $+ .db
  if (!$exists(%db)) {
    ; Can't find database, throw error
    func.debug -s SQL error: unable to load database $1
  }
  else {
    ; db exists, let's try to load it
    var %sqldb = $sqlite_open(%db)
    if (%db) {
      ; db opened successfully
      func.debug SQL: opened db %db $+, connection id: %sqldb
      return %sqldb
    }
    else {
      ; some kind of error
      func.debug -s SQL error: %sqlite_errstr
    }
  }
}

; Close database connection
; Usage: /db.close <db.number>
alias db.close {
  ; $1 is the numerical identifier returned by /db.open
  var %db = $sqlite_close($1)
  if (%db == $null) {
    func.debug -s SQL error: failed to close db $1
  }
  else {
    func.debug Closed db: $1
  }
}