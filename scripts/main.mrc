; Humphrey
; Main (settings etc.)

; Startup events
on *:START:{
  set %nick humphrey
  set %altnick humphreys
  set %channel #bob-land
  set %server zulu.diverse.net.nz
  set %debugmode $true

  ; If debug mode is on, create the debug window
  if (%debugmode == $true) {
    window @DEBUG
    func.debug humphrey started.
  }
}

; On connection to a server
on *:CONNECT:{
  join %channel
  func.debug Connected to %server as %nick
}

; On joining a channel
on *:JOIN:#:{
  debug Joined $chan on %server
}

; Exit events
on *:UNLOAD:{
  if (%debugmode == true) {
    window -c @DEBUG
  }
}
