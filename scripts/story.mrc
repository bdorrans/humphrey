; Humphrey
; Story generator
; Types of stories depend on the database

; Generate a block of text
; Usage $story.generate(<database>, <numsentences>)
alias story.generate {
  ; Variables
  var %database = $1
  var %numsentences = $2
  var %i = 1

  ; First, we need to check the database exists
  if (!$exists(db\ $+ %database)) {
    func.debug -s Error: story database db\ $+ %database does not exist.
  }

  ; Generate two sentences by default
  if (!%numsentences) { var %numsentences = 2 }

  ; Begin generation
  func.debug Generating %numsentences sentences...
  while (%i <= %numsentences) {  
    var %next = $story.sentence(%database)
    var %sentence = %sentence $+ $chr(32) $+ %next
    inc %i
  }
  func.debug Generation complete.
  return $lower(%sentence)
}

; Generate a sentence
; Usage: $story.sentence(<database>)
alias story.sentence {
  ; Variables
  var %database = $1
  var %dbpath = $mircdir $+ db\ $+ %database $+ \

  ; First, we need to check the database exists
  if (!$exists(db\ $+ %database)) {
    func.debug -s Error: story database db\ $+ %database does not exist.
  }

  ; Generate a sentence
  var %sentence = $read(%dbpath $+ sentences.txt)
  var %parsed = $story.parsesentence(%database, %sentence)
  func.debug Parsed " $+ %sentence $+ " -> " $+ %parsed $+ "

  return %parsed
}

; Generate a sentence from a specified line number in a database, instead of randomly
; Usage: $story.sentence.ln(<database>, <line number>)
alias story.sentence.ln {
  ; Variables
  var %database = $1
  var %linenum = $2
  var %dbpath = $mircdir $+ db\ $+ %database $+ \

  ; First, we need to check the database exists
  if (!$exists(db\ $+ %database)) {
    func.debug -s Error: story database db\ $+ %database does not exist.
  }

  ; Generate the specified sentence
  var %sentence = $read(%dbpath $+ sentences.txt, %linenum)
  var %parsed = $story.parsesentence(%database, %sentence)
  func.debug Parsed " $+ %sentence $+ " -> " $+ %parsed $+ "

  return %parsed
}

; Replace the keywords in the sentence
; with real things from the database
; Usage: $story.parsesentence(<database>, <sentence>)
alias story.parsesentence {
  ; Variables
  var %database = $1
  var %dbpath = $mircdir $+ db\ $+ %database $+ \
  var %sentence = $2-
  var %i = 1
  var %numwords = $numtok(%sentence, 32)

  ; Loop through every token in the sentence, if they're a keyword, replace it from the correct file
  func.debug Parsing...
  while (%i <= %numwords) {
    var %currentword = $gettok(%sentence, %i, 32)

    ; An easy check if it's a keyword
    if ($left(%currentword, 1) == [) {
      ; Strip punctuation from the keyword
      var %keyword = $remove(%currentword, [, ], $chr(46), $chr(44))
      while (%replacetext == $null) {
        var %replacetext = $read(%dbpath $+ %keyword $+ .txt)  
      }
      var %result = %result %replacetext
    }
    else {
      ; Not a keyword, just skip it
      var %result = %result %currentword
    }

    inc %i
    var %replacetext = $null
    var %keyword = $null
  }

  ; Append a period if no punctuation at the end of the sentence
  if ($right(%result, 1) != . && $right(%result, 1) != ! && $right(%result, 1) != ?) {
    var %result = %result $+ .
  } 

  func.debug Parsing complete.
  return $remove(%result, [, ])
}
