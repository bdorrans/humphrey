; Humphrey
; Poot parser
; All aliases within this should start with poot.

; Selects random tokens to transform into poots
; Usage: $poot.parse(<text>)
alias poot.parse {
  var %text = $1-
  var %parsedtext
  var %numtok = $numtok(%text, 32)

  ; Loop through the string and poot some words
  :loop
  var %i = 1
  while (%i <= %numtok) {
    if ($rand(1,10) == 1) {
      var %parsedtext = %parsedtext $poot.grammar($gettok(%text, %i, 32))
    }
    else {
      var %parsedtext = %parsedtext $gettok(%text, %i, 32)
    }
    inc %i
  }

  ; If there are no poots in the sentence, start the process again
  if (poot !isin %parsedtext) {
    unset %parsedtext
    goto loop
  }

  return %parsedtext
}

; Convert words to poots (e.g. nicely to pootly, shitting to pooting)
; Usage: $poot.grammar(<word>)
alias poot.grammar {
  var %text = $1

  ; Adverbs (ending in -ly)
  if ($regex(%text, ly\b) > 0) {
    return pootly
  }
  ; Verbs (ending in -ing)
  elseif ($regex(%text, ing\b) > 0) {
    return pooting
  }
  ; Not sures (ending in -ed)
  elseif ($regex(%text, ed\b) > 0) {
    return pooted
  }
  ; General poot
  else {
    return poot
  }
}
